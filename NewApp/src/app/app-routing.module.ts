import { Routes, RouterModule } from "@angular/router";
import { patch } from "webdriver-js-extender";
import { SessionsListComponent } from "./sessions/sessions-list/sessions-list.component";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";

const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'sessions', component: SessionsListComponent}
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(appRoutes)], //forRoot loads imported route upon page initialization
})

export class AppRoutingModule { }