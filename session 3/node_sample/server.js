var http = require('http')

var serverConfiguration = function(req, res) {
    res.writeHead(200);
    res.end("HelloWorld")
}

var server = http.createServer(serverConfiguration())

server.listen(8080);