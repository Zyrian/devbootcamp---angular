# Web Development

**Technologies:**
* Node - server-level technology (Javascript)
* Angular - front-end SPA (Single Page Architecture) framework
* Express - back-end. Used to build APIs
* MySQL
* Angular CLI - helps scaffold Angular code
* Bootstrap - styling
* TypeScript - superset of Javascript

**Language vs. Framework**
* Language - set of rules on how to build an application
* Framework - written in the language and helps simplify the process of creating an application

## Angular CLI

```
* https://cli.angular.io/
* install 
* ng new
* ng generate
* ng serve
    * compiler messages
```

**Make sure you install it correctly**
```
ng --version
```

**Creating new project**
```
ng new [Title]
    * Angular will import all dependencies that it needs and create a [Title] subfolder

ng serve
    * Application will be started - navigate to localhost:4200 on your browser
```

**Bootstrap**
```
https://getbootstrap.com/docs/3.3
CSS framework for responsive sites
rows and columns
buttons, labels, and more
```

**Emmet**
Short-hand way to creating HTML code
```
div.container>div.row>div.col-md-4*3

<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
</div>
```

**Expresion**
```
{{ myFirstVariable }}
```
* First need to import Component from @angular/core into app.component.ts
* Then create property under export class AppComponent

**One-way Binding**
```
<input type="text" [ngModel]="myFirstVariable" class="form-control">
```
* In app.module.ts, declare FormsModule in @NgModule

**Two-way Binding**
```
<input type="text" [(ngModel)]="myFirstVariable" class="form-control">
```

**Interpolation**
```
gotClicked(mfv: string): void {
    alert(`${mfv}`);
}
```
* Converts variable into a string
* Useful for concatenating the variable into a longer string

**Building a list**
```
*app.component.ts
sessions = [
    {Name: 'John Teaches Angular', Location: 'Miles-U 1'},
    {Name: 'Scott Teaches AWS', Location: 'Miles-U 2'},
    {Name: 'Jack Teaches PODIS', Location: 'Jacks Desk'}
]

*app.component.html
ul>li
<ul class="list-group">
    <li class="list-group-item" *ngFor="let session of sessions"></li> 
    //will create a list item per item in sessions array
        <div class="row">
            <div class="col-md-6">{{ session.Name }}</div>
            <div class="col-md-6">{{ session.Location }}</div>
        </div>
</ul>
```

## Routing - array of route configurations

* 1) Create new sessions-list to hold sessions
```
ng g component sessions-list
```

* 2) Create a route with the following:
```
path = URL/path of the page
component = class to be returned
```

* 3) Create app-routing-module.ts

* 4) Create router module
```
@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(appRoutes)],
})
```

* 5) Export routing module
```
export class AppRoutingModule []
```

## Nodes & Express

* Create http variable
```
var http = require('http')
```

* Initialize - Make server obj, and assign createServer to it
```
var server = http.createServer(function(req, res){
    res.writeHead(200);
    res.end("Hello World)
})
```

* Listen to the server
```
server.listen(8080);
```

* **OR** Export module
```
module.exports = app;
``` 

* Create bin folder - separate configuration from execution
```
const app = require('../app'); // identical to angular imports
```

**Express**

* Add Express to package
```
npm install --save express
```

* Initialize express variable & app
```
var express = require('express')
var app = express();
```

* Write an endpoint
```
app.get('/', (req, res) => {res.send("Hello World!!")});
```

* *Anonymous function*
```
(req, res) => {}
```

* Different endpoint methods
```
GET
POST
PUT
DELETE
```

*/**Postman** - tool used to mock client requests*
*MySQL Workbench*

**Sequelize**

* ORM - Object Relational Mapping
* Promise-based

* Install sequelize
```
npm i --save sequelize@4.13.2
```

* Create config folder with config.js
```
CONFIG = {};

CONFIG.db_dialect = 'mysql';
CONFIG.db_name = 'mytrainingrater';
CONFIG.db_user = 'root';
CONFIG.db_password = 'pass';

```

### Code First Structure 
* Create schema of your tables through the code then export it to the database

### Database First Structure
* Create database tables and schema first then import them into code

## Sequelize (continued)
* Create models folder
* Create index.js
```
'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(__filename);
var db = {};

const Op = Sequelize.Op;
const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col
};


const sequelize = new Sequelize(CONFIG.db_name, CONFIG.db_user, CONFIG.db_password, {
  host: CONFIG.db_host,
  dialect: CONFIG.db_dialect,
  operatorsAliases: operatorsAliases
});

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
```

### First Model
* Call sequelize command to access help page
* Generate model and migrations folders then run Sequelize command below
```
sequelize model:generate --name Sessions --attributes name:string,startTime:date,location:string
```

### Initialize Database
* Add configuration variable to config.js
```
CONFIG.app = 'dev';
```

* Bring config file models into app.js
```
require('./config/config');
var models = require ('./models');
```

* Add the following into app.js
```
models.sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.log('Unable to connect to the database.', err);
    });

if (CONFIG.app === 'dev') {
    models.sequelize.sync();
}
```

* In package.json, change "start" to the following. This lets you type "npm start" to initialize the server
```
    "start": "node \"bin/www.js\"",
```

## Writing Server Logic
* Create controllers folder with SessionsController.js inside it
```
const Sessions = require('../models').Sessions;

const getAll = (req, res) => {
    res.setHeader('Content-Type', 'application/json'); // return type for application
    let err, sessions; // defining variables
    sessions = [{ Name: 'John Teaches Angular', Location: 'Miles-U 1' },
    { Name: 'Scott Teaches AWS', Location: 'Miles-U 2' },
    { Name: 'Jack Teaches PODIS', Location: 'Jacks Desk' },
    ];

    return res.json(sessions); // initialize array for sessions
}

module.exports.getAll = getAll;
```

* In app.js, add the following routing
```
var sessions = require('./controllers/SessionsController')
app.get('/sessions', sessions.getAll);
```

## Example Logic
* In SessoinsController.js
```
const get = (req, res) => {
    let sessionId = parseInt(req.params.sessionId)
    res.setHeader('Content-Type', 'application/json');
    
    let sessions = 
    [{ Id: 1, Name: 'John Teaches Angular', Location: 'Miles-U 1' },
    { Id: 2, Name: 'Scott Teaches AWS', Location: 'Miles-U 2' },
    { Id: 3, Name: 'Jack Teaches PODIS', Location: 'Jacks Desk' },
    ];

    let session = sessions.find(obj => obj.Id === sessionId);
    console.log(session);
    return res.json(session);
}
module.exports.get = get;
```

* Add in app.js
```
app.get('/sessions/:sessionId', sessions.get)
```

## Promises
* Syntax - returns processed promise - 
```
to = function(promise) {
    return promise
    .then(data => {
        return [null, data];
    }).catch(err => [pe(err)])
};

require('./global_functions'); // in app.js
```

* Create async function
```
const get = async (req, res) => {
    let err, session;
    let sessionId = parseInt(req.params.sessionId)
    res.setHeader('Content-Type', 'application/json');
    
    [err, session] = await to(Sessions.findById({sessionId}))
    console.log(session);
    if (!session) { // error handling
        res.statusCode = 404;
        return res.json({ success: false, error: err});
    }
    return res.json(session);
}
module.exports.get = get;
```

*npm install --save body-parser*
*npm install --save mysql2@1.5.1*

* Add the following to app.js
```
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
```

## DB Connection Error
* One issue you may encounter when connecting your DB is an error similar to the following:
```
"Unhandled rejection SequelizeConnectionError: Client does not support authentication protocol requested by server; consider upgrading MySql client".
```
If you see this error message it is most likely due to your root user not having permission to connect to user DB's.  
* Try running the following command directly in your MySql work bench as a query or in the mysql cli:
```
"alter user 'root'@'localhost' identified with mysql_native_password by 'ROOT_PASSWORD_HERE';".  
```
This will update the permissions you need.

## Modules
* A building block of the system
* Will eventually have many modules

### We declare the following inside **Modules**
* Services
* Components
* Directives

## Lifecycle Hooks
* ngOnInit()
* ngOnDestroy()

### Observables
* A set of code that will run when asked, and will tell us when it's done
* A type of Promise
* Why do we want them? 
```
Calls to the back-end are not always as fast as we want. 
Waits for responses to be accomplished before anything can be done with the response.
Services will predominantly return Observables.
```

## HttpClient
* Create new service HttpClient on sessions.service.ts:
```
import { HttpClient } from '@angular/common/http';
constructor (
    private http: HttpClient,
) { }
```
* Update new getSessions function:
```
getSessions(): Observable<{}[]> {
    return this.http.get<{}[]>('http://localhost:3000/sessions');
  }
}
```

* On sessions-list.component.ts, adjust ngOnInit to an observable:
```
ngOnInit() {
    this.sessionsService.getSessions()
      .subscribe(
          (sessions) => this.sessions = sessions,
          );
  }
```

* Add CORS resolver function to app.js
```
// CORS
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Pass to next layer of middleware
  next();
});
```

* The sessions are still not showing up on the front-end. Why?
* Need to export interface from sessions.service.ts
```
export interface ISession {
  id: number;
  name: string;
  location: string;
  startTime: string;
  createdAt: string;
  updatedAt: string;
}
```
* Need to update all object declarations to ISession[]
```
getSessions(): Observable<ISession[]> { // in sessions.service.ts
    return this.http.get<ISession[]>('http://localhost:3000/sessions');
  }

sessions: ISession[] = []; // In sessions-list.component.ts
```

* Copy current line downward
```
Alt + Shift + Down Arrow
```

* Create new object with all properties of defaultSession
* Shallow Copy
```
const defaultSession: ISession = {
    id: 0,
    name: '',
    location: '',
    startTime: `${new Date()}`,
    createdAt: null,
    updatedAt: null,
}
    session: ISession = {...defaultSession};
```

## Summary So Far

### Peices of the back-end
* Model - database representation in code (Sequelize as ORM tool)
* Component
* Router
* Http
```
Response 
Query Params
Route Params
Body
```

## Authentication Process

* Add Hashing
* Create User Endpoint (CreateUser)
* Login Endpoint
* Handle tokens coming from front-end
```
Add Passport
Add JWT Passport
```

### Setting up users table for validation and encryption
* users.js - validation and duplicate prevention
```
npm install --save validator@9.2.0 // for isEmail function
email: { type: DataTypes.STRING, unique: true, validate: { isEmail: {msg: 'Email is invalid' } } }
phone: { type: DataTypes.STRING, allowNull: true, validate: {len: {args: 7,20}, msg: 'Phone number invalid.'}, isNumberic: { msg: 'Not a valid phone number.'}}
isTrainer: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false}
```

* bcrypt and bcrypt-promise
```
npm install --save bcrypt@2.0.0
npm install --save bcrypt-promise@2.0.0
```

* Global function for error handling
```
TE = function(errMessage, log) {
    if (log === true) {
        console.error(errMessage);
    }
    throw new Error(errMessage);
}

ReE = function(res, err, code) {
    if(typeof err == 'object' && typeof err.Message != 'undefined') {
        err = err.Message;
    }
    if (typeof code !== 'undefined') re.statusCode = code;
    return res.json({success: false, error: err});
}

ReS = function(res, data, code) {
    let sendData = {success: true};

    if(typeof data == 'object') {
        sendData = Object.assign(data, sendData);
    }

    if (typeof code !== 'undefined') res.statusCode = code;

    return res.json(sendData);
}
```

* Sequelize hooks - similar to lifecycle hooks - events that can be attached to db calls
```
* in users.js inside the var Users
const bcrypt = require('bcrypt');
const bcrypt_p = require('bcrypt-promise');
Users.beforeSave(async (user) => {
    let err;
    if (user.changed('password')) {
        let salt, hash
        [err, salt] = await to(bcrypt.genSalt(10));
        if(err) TE(err.message, true);

        [err, hash] = await to(bcrypt.hash(user.password,salt));
        if(err) TE(err.message, true);

        user.password = hash;
    }
})
```

* UsersController.js - creating functions for storing/saving users
```
const Users = require('../models').Users;
const validator = require('validator');

const create = async function (req, res) {
    res.setHeader('ContentType', 'application/json');
    const body = req.body;

    if (!body.email) {
        return ReE(res, 'Please enter an email to register', 422);
    } else if (!body.password) {
        return ReE(res, 'Please enter a password to register', 422);
    } else {
        let err, user

        [err, user] = await to(createUser(body));
        if(err) return ReE(res, err, 422);

        return ReS(res, user, 201);
    }
}
module.exports.create = create;

const createUser = async function(userInfo) {
    let err;
    if (validator.isEmail(userInfo.email)) {
        [err, user] = await to(Users.create(userInfo));
        if(err) TE('User already exists with that email');
        return user;
    } else {
        TE('Email is invalid')
    }
}
module.exports.createUser = createUser;
```

* Route for user creation
```
In app.js - 
const UserController = require('./controllers/UsersController');

app.post('/users', userController.create);
```

* Two options to recreate users table
```
1) Drop users table in existing db and recreate it
2) In app.js, set force:true in sequelize sync method
```

* Tokens
```
npm install --save passport@0.4.0
npm install --save passport-jwt@3.0.0

In config.js
CONFIG.jwt_encryption = 'assdoifajowf020jsfs'
CONFIG.jwt_expiration = '1000'

In app.js
const passport = require('passport');
app.use(passport.initialize());


```

## Authentication Flow
* 1) If no JWT, login - not protected. anyone can hit this endpoint
```
Back-end will generate JWT along with token/expiration date.
Regular endpoints are protected. Need JWT (key/passport) to access these.
```
* 2) If JWT exists, a request has to be made to access endpoint.
```
Refresh endpoint - requires you to have a valid JWT (about to expire soon). 
Verifies that JWT is valid and replaces it with a new, valid JWT.
```
* 3) Application accesses the back-end using the JWT generated by the back-end

### Back-End Security
* Secure API endpoints
```
In app.js
passport.authenticate('jwt', {session: false})
```

### Front-End Security
* Auth Service - responsible for determining isAuthenticatied and hitting login endpoint
* Secure pages or routes - should be easy to implement everywhere
* How to include security tokens when hitting endpoints from the Front-End
```
Create 'common/auth' directory inside the 'app' folder.
Create auth.service.ts in this directory.

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';

export interface ILoginResponse {
    success: boolean;
    token?: string;
}

@Injectable()
export class AuthService {

    token: string = null;

    constructor(
        private http: HttpClient,
    ) { }

    isAuthenticated(): boolean {
        return this.token ? true : false;
    }

    login(email: string, password: string): Observable<ILoginResponse> {
      const data = {
          email: email,
          password: password,
      };
      return this.http.post<ILoginResponse>('http://localhost:3000/login', data)
      .do((response) => this.token = response && response.success && response.token || null);
  }
```
```
Create auth.guard.ts in 'common/auth' directory.
*Reference Angular CanActivate function*
*Add all missing imports and add to providers array in app.module.ts*

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
      private authService: AuthService,
      private router: Router,
      private toastsManager: ToastsManager,
  ) {}
 
  canActivate(
    route: ActivatedRouteSnapshot,
  ): Observable<boolean> {
      const isAuthenticated = this.authService.isAuthenticated();
      if(!isAuthenticated) {
          this.toastsManager.error('Please login');
          this.router.navigate(['login']);
      }
      return Observable.of(isAuthenticated);
  }
}
```
```
*Blocking off routes*
In app-routing.module.ts - add canActivate to path(s)

{ path: 'sessions/:sessionId', component: SessionsDetailComponent, canActivate: [AuthGuard] },
```
```
Create login.component.ts in 'common/auth'
*Reference session-detail.component.ts*
*Add LoginComponent to appRoutes*

@Component({
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

    email = '';
    password = '';

  constructor(
    private router: Router,
    private toastsManager: ToastsManager,
  ) { }

  ngOnInit() {
  }

  login(): void {
      this.authService.login(this.email,this.password)
      .subscribe(
          (response) => {
              if (response.success) {
                  this.toastsManager.success(response.token);
              } else {
                  this.toastsManager.error('is no working');
              }
          },
          (error) => {
            this.toastsManager.error('is no working');
          }
      );
  }
}

Create login.component.html
```

* Adding Header to Http:get - to show content on the page
```
HTTP_INTERCEPTOR
* Add Bearer to every call

Create token.interceptor.ts in common/auth directory
*Reference Angular.io HttpInterceptor*
*Add {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true} to providers in app.module.ts*

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    private authService: AuthService;

    constructor(private injector: Injector) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        if (!this.authService) {
            this.authService = this.injector.get(AuthService); // anything in the providers statement should only be called once
        }
        
        const token = this.authService.token;

        if (token) {
            const headers = {
                'Authorization': token,
            };

            const dupRequest = request.close({
                setHeaders: headers,
            });

            return next.handle(dupRequest);
        } else {
            return next.handle(request);
        }

        
    }
}
```

### Stretch Goals
* Make password great again

### Concepts
* What if I want to know who I am as a user?
```
AuthService
* IsAuthenticated?
* token
* CurrentUser - Behavor Subject (rxjs library)
* Sign Out
```
* Keeping users logged in. Storing sessions in cookies
```
* Ngx-cookie
```
## Session 9 Plan
* Add Ratings Logic
* Add Associations to Session
* Add Ratings CRUD
* Update Sessions Controller
* Pull Average Rating

* Hook Ratings to Back-End
* Display Average Rating

### Adding Ratings Model
```
sequelize model:generate --name Ratings --attributes rating:decimal,sessionId: integer
```
* In ratings.js. Will need to DROP TABLES when creating associations.
```
module.exports = (sequelize, DataTypes) => {
    var Ratings = sequelize.define('Ratings', {
        rating: {type: DataTypes.DECIMAL, allowNull: false, min: {
            args: [0],
            msg: 'Rating must be greater than 0'
        }, max: {
            args: [5],
            msg: 'Rating must be less than or equal to 5.'
        }
        },
    });
    Ratings.associate = function(models) {
        Ratings.belongsTo(models.Sessions, {foreignKey: 'sessionId', sourceKey: 'id'});
    }
```
* Define relationship for sessions and ratings. In sessions.js
```
    var Sessions... {
    Sessions.associate = function(models) {
        models.Sessions.hasMany(models.Ratings, {foreignKey: 'sessionId', sourceKey: 'id'});
    } return Sessions;
};
```
* Create RatingsController.js
```
const Ratings = requre('../models').Ratings;

const create async function(req, res) {
    res.setHeader('Content-Type', 'Application/json');
    let err, rating, ratingInfo, sessionId;

    sessionId = req.params.sessionId;
    ratingInfo = req.body;

    ratingInfo.sessionId = sessionId;
    // ADD VALIDATION ON RATING DECIMAL AND SESSIONID
    [err, rating] await to(Ratings.create(ratingInfo));
    if(err) return ReE(res, err, 422);

    return ReS(res, rating, 201);
}
module.exports.create = create;
```
* Add route in app.js
```
const ratings = require('./controllers/RatingsController.js');
app.post('/ratings/:sessionId', passport.authenticate('jwt', {session: false}), ratingsController.create);
```
* Make update function for ratings
```
const update async function(req, res) {
    let err, rating, ratingInfo, ratingId;
    ratingId = req.params.ratingId;
    ratingInfo = req.body;

[err, rating] = await to(Ratings.update({ rating: ratingInfo.rating }, { where: { id: ratingId }}));
if (err) return ReE(res, err, 422);

return ReS(res, rating, 200);
}
module.exports.update = update;

app.put('/ratings/:ratingId', ratings.update);
```
* Average ratings - SessionsController.js
```
* In getAll method *
for (let i in sessions) {
    let sessionsInfo = sessions[i].toJSON();
    sessionInfo.avgRating = 0;;
    for (let r in sessionInfo.Ratings) {
        sessionInfo.avgRating += parseInt{sessionInfo.Ratings[r].rating};
    }
    if (sessionInfo.Ratings.length > 0) {
        sessionInfo.avgRating = sessionInfoavgRating / sessionInfo.Ratings.length;
    }
    
}
```